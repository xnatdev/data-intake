/**
 * 
 */
package com.xnat.aggregatorapp.entity;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Patient {
	private String patientId;
	private String patientName;
	private String patientGender;
	private String patientAge;
	private double patientWeight;
}
