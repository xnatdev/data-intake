/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.data.ResourceRecord
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package com.xnat.aggregatorapp.entity;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(schema = "xnat", name = "resource_record")
public class ResourceRecord extends BaseMetadata {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6274987970410545712L;

	@NonNull
    @Column(unique = true)
	@Lob
    private String location;

    private String format;

    @Temporal(TIMESTAMP)
    private Date extractionStartTime;

    @Temporal(TIMESTAMP)
    private Date extractionEndTime;

    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode processor;

    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode metadata;

    private String referenceType;

    private long referenceId;
}
