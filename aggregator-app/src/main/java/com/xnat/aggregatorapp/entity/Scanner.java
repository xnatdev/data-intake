/**
 * 
 */
package com.xnat.aggregatorapp.entity;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Scanner {
	private String manufacturer;
    private String model;
    private String softwareVersion;
}
