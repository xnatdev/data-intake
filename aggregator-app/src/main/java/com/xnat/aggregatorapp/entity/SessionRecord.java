/**
 * 
 */
package com.xnat.aggregatorapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */

//@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
//@Table(schema = "xnat", name = "aggregator_entity")
public class SessionRecord extends BaseMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5020916628084074497L;

	private String studyInstanceUid;
	private String studyId;
	private String studyDescription;
	private String accessionNumber;
	private String sessionType;
	private String subjectId;
	private Patient patient;
	private String date;
	private String time;
	private String acquisitionSite;
	private Scanner scanner;
	private String institutionName;
	private String transferSyntaxUid;
//	private List<Integer> scans;

}
