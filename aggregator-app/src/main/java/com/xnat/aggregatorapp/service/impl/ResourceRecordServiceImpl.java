package com.xnat.aggregatorapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnat.aggregatorapp.entity.ResourceRecord;
import com.xnat.aggregatorapp.repository.ResourceRecordRepository;
import com.xnat.aggregatorapp.service.ResourceRecordService;

@Service
public class ResourceRecordServiceImpl implements ResourceRecordService {

	@Autowired
	ResourceRecordRepository resourceRecordRepository;
	
	@Override
	public ResourceRecord createResourceRecord(ResourceRecord resourceRecord) {
	return resourceRecordRepository.save(resourceRecord);
	}

	@Override
	public ResourceRecord findResourceRecordById(Long id) {
	return resourceRecordRepository.getOne(id);
	}

	@Override
	public List<ResourceRecord> getAllResourceRecords() {
		return resourceRecordRepository.findAll();
	}
}
