/**
 * 
 */
package com.xnat.aggregatorapp.service;

import com.xnat.aggregatorapp.entity.ResourceRecord;
import com.xnat.aggregatorapp.entity.SessionRecord;

/**
 * @author afour
 *
 */
public interface AggregatorService {
//	public void listen(final ResourceRecord resourceRecord) throws ValueNotFoundException;
	
	SessionRecord convertResourceRecordtoAggregatorEntity(ResourceRecord resourceRecord);

//	SessionRecord findAggregatorEntityById(Long id);

	void checkForNewDatabaseEntries();
}
