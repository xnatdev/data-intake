package com.xnat.aggregatorapp.service;

import java.util.List;

import com.xnat.aggregatorapp.entity.ResourceRecord;

public interface ResourceRecordService {

	ResourceRecord createResourceRecord(ResourceRecord resourceRecord);

	ResourceRecord findResourceRecordById(Long id);
	
	List<ResourceRecord> getAllResourceRecords();
  
  }
 