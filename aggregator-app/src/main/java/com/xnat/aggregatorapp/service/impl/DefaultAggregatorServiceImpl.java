/**
 * 
 */
package com.xnat.aggregatorapp.service.impl;

import static com.xnat.aggregatorapp.constants.DicomExtractorConstants.AGGREGATE_QUEUE_EVENT;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.xnat.aggregatorapp.LaunchCommand;
import com.xnat.aggregatorapp.dto.AggregatorOutput;
import com.xnat.aggregatorapp.entity.Patient;
import com.xnat.aggregatorapp.entity.ResourceRecord;
import com.xnat.aggregatorapp.entity.Scanner;
import com.xnat.aggregatorapp.entity.SessionRecord;
import com.xnat.aggregatorapp.service.AggregatorService;
import com.xnat.aggregatorapp.service.ResourceRecordService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author afour
 *
 */
@Slf4j
@Service
public class DefaultAggregatorServiceImpl implements AggregatorService {

//	private final String cron =  "*/15 * * * *";
//	private final String fixedRate = "900000";
	private final String fixedRate = "60000";
	private ObjectMapper _objectMapper;
	private ObjectNode _processor;
	private RabbitTemplate _rabbitTemplate;
	ResourceRecordService _resourceRecordService;

	@Autowired
	public DefaultAggregatorServiceImpl(final RabbitTemplate rabbitTemplate,
			final Jackson2ObjectMapperFactoryBean objectMapperBuilderFactory, final LaunchCommand command,
			ResourceRecordService resourceRecordService) {
		_rabbitTemplate = rabbitTemplate;
		_resourceRecordService = resourceRecordService;
		_objectMapper = Objects.requireNonNull(objectMapperBuilderFactory.getObject());
		_processor = _objectMapper.createObjectNode();
		_processor.put("implementation", getClass().getName());
		command.call();
	}

//	  @Scheduled(cron = cron )
	@Scheduled(fixedRateString = fixedRate)
	@Override
	public void checkForNewDatabaseEntries() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -1);
		calendar.add(Calendar.SECOND, -1);
		Date lastCheckedDate = calendar.getTime();

		List<ResourceRecord> records = _resourceRecordService.getAllResourceRecords();

		if (records != null && !records.isEmpty()) {
			List<AggregatorOutput> outputs = records.stream().filter(r -> r.getCreated().after(lastCheckedDate))
					.map(r -> new AggregatorOutput(r.getId(), getJsonNodeValue(r.getMetadata(), "0020000D")))
					.collect(Collectors.toList());

//			log.info("AggregatorOutput : {}", outputs);

			outputs.forEach(ao -> {
				log.info("AggregatorOutput : {}", ao);
				_rabbitTemplate.convertAndSend(AGGREGATE_QUEUE_EVENT, ao);
			});
		}
	}

	/*
	 * @RabbitListener(queues = EXTRACT_QUEUE_EVENT) public void listen(final
	 * ResourceRecord resourceRecord) throws ValueNotFoundException {
	 * log.info("Just got a record from the queue {}: {}", EXTRACT_QUEUE_EVENT,
	 * resourceRecord);
	 * 
	 * SessionRecord entity =
	 * convertResourceRecordtoAggregatorEntity(resourceRecord);
	 * 
	 * _rabbitTemplate.convertAndSend(AGGREGATE_QUEUE_EVENT, entity); }
	 */
	@Override
	public SessionRecord convertResourceRecordtoAggregatorEntity(ResourceRecord resourceRecord) {

		JsonNode jsonNode = resourceRecord.getMetadata();

		SessionRecord aggregatorEntity = new SessionRecord();

		aggregatorEntity.setStudyInstanceUid(getJsonNodeValue(jsonNode, "0020000D"));
		aggregatorEntity.setStudyDescription(getJsonNodeValue(jsonNode, "00081030"));
		aggregatorEntity.setAccessionNumber(getJsonNodeValue(jsonNode, "00080050"));

		aggregatorEntity.setDate(getJsonNodeValue(jsonNode, "00080020"));
		aggregatorEntity.setTime(getJsonNodeValue(jsonNode, "00080030"));

		Patient patient = new Patient(getJsonNodeValue(jsonNode, "00100020"),
				jsonNode.findPath("00100010").findValue("Value").findValue("Alphabetic").asText(),
				getJsonNodeValue(jsonNode, "00100040"), getJsonNodeValue(jsonNode, "00101010"),
				jsonNode.findPath("00101030").findValue("Value").path(0).asDouble(0.00));
		aggregatorEntity.setPatient(patient);

		Scanner scanner = new Scanner(getJsonNodeValue(jsonNode, "00080070"), getJsonNodeValue(jsonNode, "00081090"),
				getJsonNodeValue(jsonNode, "00181020"));
		aggregatorEntity.setScanner(scanner);

//		aggregatorEntity.setStudyId(jsonNode.findPath("00200010").findValue("Value").path(0).asText());
//		aggregatorEntity.setSessionType(jsonNode.findPath("0020000D").findValue("Value").path(0).asText());Not Present in dicom file
//		aggregatorEntity.setSubjectId(jsonNode.findPath("0020000D").findValue("Value").path(0).asText());Not Present in dicom file

//		aggregatorEntity.setAcquisitionSite(acquisitionSite); Not Present in dicom file, MRAcquisitionType or AcquisitionMatrix
//		aggregatorEntity.setInstitutionName(institutionName); Not Present in dicom file

//		aggregatorEntity.setTransferSyntaxUid(jsonNode.findPath("00020010").findValue("Value").path(0).asText()); not present in resource record

		aggregatorEntity.setCreated(new Date());
		aggregatorEntity.setLastModified(new Date());

		return aggregatorEntity;
//				_aggregatorRepository.save(aggregatorEntity);
	}

	/*
	 * @Override public SessionRecord findAggregatorEntityById(Long id) { return
	 * _aggregatorRepository.getOne(id); }
	 */

	private String getJsonNodeValue(JsonNode node, String nodeId) {

		final JsonNode nodeData = node.findPath(nodeId);

		if (nodeData != null) {
			final JsonNode nodeValue = nodeData.findValue("Value").path(0);

			return nodeValue.asText("value not found");
		}
		return "";
	}
}
