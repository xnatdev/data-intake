/**
 * 
 */
package com.xnat.aggregatorapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author afour
 *
 */
@Data
@AllArgsConstructor
public class AggregatorOutput {
	Long id;
	String studyInstanceUid;
}
