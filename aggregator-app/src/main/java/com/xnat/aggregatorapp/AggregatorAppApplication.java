package com.xnat.aggregatorapp;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableRabbit
@SpringBootApplication
public class AggregatorAppApplication {

	public static void main(String[] arguments) {
		SpringApplication application = new SpringApplication(AggregatorAppApplication.class);
		application.run(arguments);
	}

}
