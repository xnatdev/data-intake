package com.xnat.aggregatorapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xnat.aggregatorapp.entity.ResourceRecord;


@Repository
public interface ResourceRecordRepository extends JpaRepository<ResourceRecord, Long> {

}
