package com.xnat.sentryapp.constants;

public class DicomCaptureStoreConstants {
 public static final String HOSTNAME="127.0.0.1";
 public static final String DICOM_FOLDER_EXCEPTION_MESSAGE="An error occurred trying to create a temporary directory under the download folder ";
 public static final String DICOM_FILE_EXCEPTION_MESSAGE="An error occurred trying to store incoming data to the file ";
 public static final String DICOM_RENAME_FILE_EXCEPTION_MESSAGE= "An error occurred trying to move an incoming DICOM file from ";
 public static final String DICOM_RENAME_FILE_TO=" to ";
 public static final String DICOM_RENAME_FILE_EXCEPTION=". Leaving download file for forensics purposes.";
 public static final String DICOM_READ_FILE_EXCEPTION_MESSAGE="An error occurred trying to read incoming DICOM data";

 public static final String ATTRIBUTE_NAME="name";
 public static final String ATTRIBUTE_URI="uri";
 public static final String ATTRIBUTE_SIZE="size";
 public static final String ATTRIBUTE_PATH="path";
 public static final String ATTRIBUTE_UPDATED="updated";
 
 public static final String ROOT_DIR = "/home/afour/XNAT/images";
 
 
}
