package com.xnat.sentryapp.constants;

public class ApplicationRunnerConstants {

	public static final String ARGUMENT_OPTION_MODE_EMPTY="You must provide a mode with the --mode=<MODE> command-line parameter.";
	public static final String ARGUMENT_OPTION_MODE_NOT_EMPTY="You must provide only one mode with the --mode=<MODE> command-line parameter.";

	public static final String ARGUMENT_OPTION_AE_TITLE_EMPTY="You must provide an AE title with the --ae-title=<AE> command-line parameter.";
	public static final String ARGUMENT_OPTION_AE_TITLE_NOT_EMPTY="You must provide only one AE title with the --ae-title=<AE> command-line parameter.";
	
	public static final String ARGUMENT_OPTION_PORT_EMPTY="You must provide an port with the --port=<PORT> command-line parameter.";
	public static final String ARGUMENT_OPTION_PORT_NOT_EMPTY="You must provide only one port with the --port=<PORT> command-line parameter.";
	
	public static final String ARGUMENT_OPTION_SERVICE_EMPTY="You must provide service name with the --service-name=<SN> command-line parameter.";
	public static final String ARGUMENT_OPTION_SERVICE_NOT_EMPTY="You must provide only one provide service name with the --service-name=<SN> command-line parameter.";
}
