package com.xnat.sentryapp.constants;

public class ModeConstants {
	public static final String MODE_PREFIX = "-m";
	public static final String MODE_TITLE = "--mode";
	public static final String MODE_PARAM_LABLE = "<MODE>";
	public static final String MODE_DESCRIPTION = "Specifies the mode for this application. The available modes are \"capture\", \"extract\", and \"aggregate\".";
	public static final String MODE_DEFAULT_VALUE = "capture";
	public static final String MODE_TITLE_WITHOUT_PREFIX = "mode";
	
	public static final String AE_TITLE_PREFIX = "-aet";
	public static final String AE_TITLE = "--ae-title";
	public static final String AE_TITLE_PARAM_LABLE = "<AE>";
	public static final String AE_TITLE_DEFAULT_VALUE = "XNAT";
	public static final String AE_TITLE_DESCRIPTION = "Specifies the AE title for the DICOM receiver. Defaults to \"XNAT\".";
	public static final String AE_TITLE_WITHOUT_PREFIX = "ae-title";
	
	public static final String PORT_PREFIX = "-p";
	public static final String POST_TITLE = "--port";
	public static final String PORT_PARAM_VALUE = "<PORT>";
	public static final String PORT_DEFAULT_VALUE = "XNAT";
	public static final String PORT_DESCRIPTION = "Specifies the port for the DICOM receiver. Defaults to 8104.";
	public static final String PORT_TITLE_WITHOUT_PREFIX = "port";
	public static final Integer PORT_VALUE = 8104;
	
	public static final String SERVICE_PREFIX = "-s";
	public static final String SERVICE_TITLE = "--service";
	public static final String SERVICE_PARAM_LABLE = "<SN>";
	public static final String SERVICE_DEFAULT_VALUE = "cStore";
	public static final String SERVICE_DESCRIPTION = "Specifies the service name for the DICOM receiver. Defaults to \"cStore\".";

	public static final String DESTINATION_PREFIX = "0";
	public static final String DESTINATION_PARAM_LABLE = "<FOLDER>";
	public static final String DESTINATION_DEFAULT_VALUE = "store";
	public static final String DESTINATION_DESCRIPTION = "The folder where incoming data should be stored.";
	
	public static final String LAUNCH_COMMAND_NAME = "launch";
	public static final String LAUNCH_COMMAND_DESCRIPTION = "Launches the DICOM C-STORE receiver";
	public static final boolean LAUNCH_COMMAND_OPTION = true;

}
