package com.xnat.sentryapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xnat.sentryapp.entity.CaptureRecord;

@Repository
public interface CaptureRecordRepository extends JpaRepository<CaptureRecord, Long> {

}
