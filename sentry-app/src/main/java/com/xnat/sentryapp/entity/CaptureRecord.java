/*
 * dicom-store: org.nrg.xnat.dicom.workers.store.data.CaptureRecord
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package com.xnat.sentryapp.entity;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(schema = "xnat", name = "capture_record")
public class CaptureRecord extends BaseMetadata {
	private static final long serialVersionUID = -4338345803820494466L;
	@NonNull
    @Column(unique = true)
	@Lob
    private String location;
	
    private String format;
    
    @Temporal(TIMESTAMP)
    private Date captureStartTime;
    
    @Temporal(TIMESTAMP)
    private Date captureEndTime;
    
    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode processor;
    
    @Type(type = "JsonB")
    @Column(columnDefinition = "JsonB")
    private JsonNode attributes;
}
