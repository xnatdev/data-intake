package com.xnat.sentryapp.config;

import java.nio.file.Path;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import com.xnat.sentryapp.constants.ModeConstants;
import com.xnat.sentryapp.dto.Mode;

@Builder
@Getter
@Accessors(prefix = "_")
@Slf4j
@Command(name = ModeConstants.LAUNCH_COMMAND_NAME, description = ModeConstants.LAUNCH_COMMAND_DESCRIPTION, mixinStandardHelpOptions = true)
public class LaunchCommand implements Callable<Integer> {
	
	@Option(names = { ModeConstants.MODE_PREFIX,ModeConstants.MODE_TITLE }, paramLabel = ModeConstants.MODE_PARAM_LABLE, defaultValue = ModeConstants.MODE_DEFAULT_VALUE, description = ModeConstants.MODE_DESCRIPTION)
	private final Mode _mode;

	@Option(names = { ModeConstants.AE_TITLE_PREFIX,ModeConstants.AE_TITLE }, paramLabel = ModeConstants.AE_TITLE_PARAM_LABLE, defaultValue = ModeConstants.AE_TITLE_DEFAULT_VALUE, description = ModeConstants.AE_TITLE_DESCRIPTION)
	private final String _aeTitle;

	@Option(names = { ModeConstants.PORT_PREFIX, ModeConstants.POST_TITLE }, paramLabel = ModeConstants.PORT_DEFAULT_VALUE, defaultValue = ModeConstants.PORT_DEFAULT_VALUE, description = ModeConstants.PORT_DESCRIPTION)
	private final int _port;

	@Option(names = { ModeConstants.SERVICE_PREFIX, ModeConstants.SERVICE_TITLE }, paramLabel = ModeConstants.SERVICE_PARAM_LABLE, defaultValue = ModeConstants.SERVICE_DEFAULT_VALUE, description = ModeConstants.SERVICE_DESCRIPTION)
	private final String _serviceName;

	@Parameters(index = ModeConstants.DESTINATION_PREFIX, paramLabel = ModeConstants.DESTINATION_PARAM_LABLE, defaultValue = ModeConstants.DESTINATION_DEFAULT_VALUE, description = ModeConstants.DESTINATION_DESCRIPTION)
	private final Path _destination;

	@Override
	public Integer call() throws Exception {
		log.info("The launch command was called with the following options and parameters:");
		switch (getMode()) {
		case capture:
			log.info(" * Mode:         capture");
			log.info(" * AE title:     {}", StringUtils.defaultIfBlank(getAeTitle(), "<NA>"));
			log.info(" * Port:         {}", getPort() != 0 ? getPort() : "<NA>");
			log.info(" * Service name: {}", StringUtils.defaultIfBlank(getServiceName(), "<NA>"));
			log.info(" * Destination:  {}", getDestination() != null ? getDestination() : "<NA>");
			break;
		case extract:
			log.info(" * Mode: extract");
			break;
		case aggregate:
			log.info(" * Mode: aggregate");
			break;
		case rest:
			log.info(" * Mode: rest");
			break;
		}
		return 0;
	}

}
