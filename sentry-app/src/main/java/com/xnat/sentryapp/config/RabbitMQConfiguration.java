package com.xnat.sentryapp.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

	@Value("${spring.rabbitmq.capture.event}")
	private String captureQueueEvent;

	@Value("${spring.rabbitmq.rest.event}")
	private String restQueueEvent;

	@Value("${spring.rabbitmq.dlx.event}")
	private String dlxQueueEvent;

	@Bean
	public Queue captureQueue() {
		return new Queue(captureQueueEvent, true);
	}

	@Bean
	public Queue restQueue() {
		return new Queue(restQueueEvent, true);
	}

	@Bean
	public Queue dlxQueue() {
		return new Queue(dlxQueueEvent, true);
	}
	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

}
