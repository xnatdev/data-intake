package com.xnat.sentryapp.config;

import java.nio.file.Paths;
import java.util.List;

import com.xnat.sentryapp.constants.ModeConstants;
import com.xnat.sentryapp.dto.Mode;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class DicomStoreServiceConfiguration {
	
	private final LaunchCommand _command;

	@Autowired
	public DicomStoreServiceConfiguration(final ApplicationArguments arguments) {
		log.info("Creating LaunchCommand from application arguments: {}",
				StringUtils.join(arguments.getSourceArgs(), " "));
		final Mode mode= Mode.valueOf(arguments.containsOption(ModeConstants.MODE_TITLE_WITHOUT_PREFIX) ? arguments.getOptionValues(ModeConstants.MODE_TITLE_WITHOUT_PREFIX).get(0) : ModeConstants.MODE_DEFAULT_VALUE);
		final String aeTitle = arguments.containsOption(ModeConstants.AE_TITLE_WITHOUT_PREFIX) ? arguments.getOptionValues(ModeConstants.AE_TITLE_WITHOUT_PREFIX).get(0): ModeConstants.AE_TITLE_DEFAULT_VALUE; 
		final int port = arguments.containsOption(ModeConstants.PORT_TITLE_WITHOUT_PREFIX) ? Integer.parseInt(arguments.getOptionValues(ModeConstants.PORT_TITLE_WITHOUT_PREFIX).get(0)): ModeConstants.PORT_VALUE;
		final List<String> nonOptionArgs = arguments.getNonOptionArgs();
		final String destination = nonOptionArgs.isEmpty() ?ModeConstants.DESTINATION_DEFAULT_VALUE : nonOptionArgs.get(0);
		
		_command = LaunchCommand.builder().mode(mode).aeTitle(aeTitle).port(port).destination(Paths.get(destination)).build();
	}

	@Bean
	public LaunchCommand launchCommand() {
		return _command;
	}
}
