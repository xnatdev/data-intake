package com.xnat.sentryapp.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

import com.xnat.sentryapp.constants.ApplicationRunnerConstants;
import com.xnat.sentryapp.exception.IllegalArgumentException;

@Configuration
public class ApplicationRunnerConfiguration implements ApplicationRunner {

	@Value("${spring.application.runner.mode}")
	private String argumentOptionMode;

	@Value("${spring.application.runner.aetitle}")
	private String argumentOptionAETitle;

	@Value("${spring.application.runner.port}")
	private String argumentOptionPort;

	@Value("${spring.application.runner.service.name}")
	private String argumentOptionServiceName;

	@Override
	public void run(ApplicationArguments arguments) throws Exception {
		List<String> argumentOptions = new ArrayList<String>();
		if (arguments.containsOption(argumentOptionMode)) {
			argumentOptions = arguments.getOptionValues(argumentOptionMode);
			if (argumentOptions.isEmpty())
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_MODE_EMPTY);
			else if (argumentOptions.size() > 1)
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_MODE_NOT_EMPTY);
		}
		if (arguments.containsOption(argumentOptionAETitle)) {
			argumentOptions = arguments.getOptionValues(argumentOptionAETitle);
			if (argumentOptions.isEmpty())
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_AE_TITLE_EMPTY);
			else if (argumentOptions.size() > 1)
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_AE_TITLE_NOT_EMPTY);
		}
		if (arguments.containsOption(argumentOptionPort)) {
			argumentOptions = arguments.getOptionValues(argumentOptionPort);
			if (argumentOptions.isEmpty())
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_PORT_EMPTY);
			else if (argumentOptions.size() > 1)
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_PORT_NOT_EMPTY);
		}
		if (arguments.containsOption(argumentOptionServiceName)) {
			argumentOptions = arguments.getOptionValues(argumentOptionServiceName);
			if (argumentOptions.isEmpty())
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_SERVICE_EMPTY);
			else if (argumentOptions.size() > 1)
				throw new IllegalArgumentException(ApplicationRunnerConstants.ARGUMENT_OPTION_SERVICE_NOT_EMPTY);
		}
	}
}
