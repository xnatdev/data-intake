package com.xnat.sentryapp.service;

import com.xnat.sentryapp.entity.CaptureRecord;

public interface CaptureRecordService{


public CaptureRecord createCaptureRecord(CaptureRecord captureRecord);

public CaptureRecord findCaptureRecordById(Long id);	
	
}
