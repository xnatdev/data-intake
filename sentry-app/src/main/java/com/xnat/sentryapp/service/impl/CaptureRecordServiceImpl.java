package com.xnat.sentryapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnat.sentryapp.entity.CaptureRecord;
import com.xnat.sentryapp.repository.CaptureRecordRepository;
import com.xnat.sentryapp.service.CaptureRecordService;

@Service
public class CaptureRecordServiceImpl implements CaptureRecordService {

	@Autowired
	CaptureRecordRepository captureRecordRepository;
	
	@Override
	public CaptureRecord createCaptureRecord(CaptureRecord captureRecord) {
	return captureRecordRepository.save(captureRecord);
	}

	@Override
	public CaptureRecord findCaptureRecordById(Long id) {
	return captureRecordRepository.getOne(id);
	}
}
