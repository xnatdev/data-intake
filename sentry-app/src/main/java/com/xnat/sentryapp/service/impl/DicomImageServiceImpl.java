package com.xnat.sentryapp.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xnat.sentryapp.constants.DicomCaptureStoreConstants;
import com.xnat.sentryapp.dto.DicomImage;
import com.xnat.sentryapp.service.DicomImageService;
import com.xnat.sentryapp.util.StoreFileUtil;

@Service
public class DicomImageServiceImpl implements DicomImageService {
	@Value("${spring.rabbitmq.rest.event}")
	private String restQueueEvent;
	
	@Autowired
	private RabbitTemplate _rabbitTemplate;

	@Override
	public String sendDicomImagesMessage(MultipartFile file) throws IOException {
		File downloadedFile = StoreFileUtil.saveFileLocalStorgae(file,DicomCaptureStoreConstants.ROOT_DIR);
		DicomImage dicomImage = new DicomImage();
		dicomImage.setContenttype(file.getContentType());
		dicomImage.setImgByte(file.getBytes());
		dicomImage.setImageName(file.getOriginalFilename());
		String path = downloadedFile.getAbsolutePath();
		dicomImage.setLocation(path);
		_rabbitTemplate.convertAndSend(restQueueEvent, path);
		
		return path;
	}

}
