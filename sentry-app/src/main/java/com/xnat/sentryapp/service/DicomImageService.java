package com.xnat.sentryapp.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface DicomImageService {
	public String sendDicomImagesMessage(MultipartFile file) throws IOException;
}
