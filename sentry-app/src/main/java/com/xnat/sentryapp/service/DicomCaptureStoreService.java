package com.xnat.sentryapp.service;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.DicomServiceException;
import com.xnat.sentryapp.entity.CaptureRecord;

public interface DicomCaptureStoreService {
	CaptureRecord capture(final Association incoming, final PresentationContext context, final Attributes rq,final PDVInputStream data, final Attributes rsp) throws DicomServiceException;
}
