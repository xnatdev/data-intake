package com.xnat.sentryapp.exception;

import java.io.Serializable;

public class IllegalArgumentException extends RuntimeException implements Serializable {
	private static final long serialVersionUID = -3752668946110859013L;

	public IllegalArgumentException(String message) {
		super(message);
	}

	public IllegalArgumentException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public IllegalArgumentException(Throwable throwable) {
		super(throwable);
	}
}
