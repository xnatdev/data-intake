package com.xnat.sentryapp.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class StoreFileUtil {

	public static File saveFileLocalStorgae(MultipartFile file, String path) throws IOException {
		String filePath = "";
		File saveFile = new File(filePath);
		byte[] bytes = file.getBytes();
		if (bytes == null || bytes.length <= 0) {
			throw new FileNotFoundException();
		}
		try {
			/* Create folder if doesn't exist */
			File folder = new File(path);
			if (!folder.exists()) {
				folder.mkdir();
			}
			/* Write file to specified location */
			saveFile = new File(folder.getAbsolutePath() + File.separator + file.getOriginalFilename());
			FileOutputStream fileOutputStream = new FileOutputStream(saveFile);
			fileOutputStream.write(bytes, 0, bytes.length);
			filePath = saveFile.getAbsolutePath();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return saveFile;
	}
}
