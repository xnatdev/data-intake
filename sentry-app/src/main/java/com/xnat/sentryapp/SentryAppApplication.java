package com.xnat.sentryapp;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class SentryAppApplication {

	public static void main(String[] arguments) {
		SpringApplication application = new SpringApplication(SentryAppApplication.class);
		application.run(arguments);
	}
}
