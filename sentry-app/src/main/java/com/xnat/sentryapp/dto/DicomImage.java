package com.xnat.sentryapp.dto;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.Data;
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@Data
public class DicomImage implements Serializable{
	private static final long serialVersionUID = 6125057752557482462L;
	private Long imageId;
	private String imageName;
	private byte[] imgByte;
	private String Contenttype;
	private String Location;
}
