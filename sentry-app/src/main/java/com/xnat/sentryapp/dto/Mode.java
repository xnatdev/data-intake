package com.xnat.sentryapp.dto;

import lombok.Getter;

@Getter
public enum Mode {
	capture, extract, aggregate, rest
}
