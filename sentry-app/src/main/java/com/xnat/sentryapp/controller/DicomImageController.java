package com.xnat.sentryapp.controller;


import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.xnat.sentryapp.service.DicomImageService;

@RestController
@RequestMapping("/dicom/images")
public class DicomImageController {

	@Autowired
	private DicomImageService dicomImageService;

	@PostMapping(value = "/send")
	public String producerDicomImages(@RequestParam("imageFile") MultipartFile file) throws IOException {
		String path = dicomImageService.sendDicomImagesMessage(file);
		return "Message sent to the RabbitMQ Successfully " + path;
	}
}
