package com.xnat.extractorapp.constants;

public class DicomExtractorConstants {
	public static final String EXTRACT_QUEUE_EVENT="extract";
	public static final String DLX_QUEUE_EVENT="DeadLetterExchange";
	public static final String CAPTURE_QUEUE_EVENT="capture";
	public static final String DICOM_FORMAT="DICOM";
	public static final Integer STRING_BUFFER_SIZE=1024 * 1024;
	public static final String FILE_START_TEXT="file://";

}
