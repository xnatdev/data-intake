package com.xnat.extractorapp.service;

import com.xnat.extractorapp.dto.DicomImage;
import com.xnat.extractorapp.entity.CaptureRecord;
import com.xnat.extractorapp.entity.ResourceRecord;

public interface DicomExtractorService{

	ResourceRecord extract(final CaptureRecord captureRecord);

	void extractToQueue(DicomImage dicomImage);
}
