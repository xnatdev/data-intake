package com.xnat.extractorapp.service;

import com.xnat.extractorapp.entity.ResourceRecord;

public interface ResourceRecordService {

	ResourceRecord createResourceRecord(ResourceRecord resourceRecord);

	ResourceRecord findResourceRecordById(Long id);
  
  }
 