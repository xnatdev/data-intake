package com.xnat.extractorapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnat.extractorapp.entity.ResourceRecord;
import com.xnat.extractorapp.repository.ResourceRecordRepository;
import com.xnat.extractorapp.service.ResourceRecordService;

@Service
public class ResourceRecordServiceImpl implements ResourceRecordService {

	@Autowired
	ResourceRecordRepository resourceRecordRepository;
	
	@Override
	public ResourceRecord createResourceRecord(ResourceRecord resourceRecord) {
	return resourceRecordRepository.save(resourceRecord);
	}

	@Override
	public ResourceRecord findResourceRecordById(Long id) {
	return resourceRecordRepository.getOne(id);
	}
}
