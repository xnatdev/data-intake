package com.xnat.extractorapp.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.stream.JsonGenerator;

import org.apache.commons.lang3.time.StopWatch;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.json.JSONWriter;
import org.dcm4che3.util.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.xnat.extractorapp.LaunchCommand;
import com.xnat.extractorapp.constants.DicomExtractorConstants;
import com.xnat.extractorapp.dto.DicomImage;
import com.xnat.extractorapp.entity.CaptureRecord;
import com.xnat.extractorapp.entity.ResourceRecord;
import com.xnat.extractorapp.service.DicomExtractorService;
import com.xnat.extractorapp.service.ResourceRecordService;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class DicomExtractorServiceImpl implements DicomExtractorService {

	@Value("${spring.rabbitmq.extract.event}")
	private String _extractQueueEvent;

	@Value("${spring.rabbitmq.dlx.event}")
	private String _dlxQueueEvent;

	@Value("${spring.rabbitmq.capture.event}")
	private String _captureQueueEvent;

	private final ResourceRecordService _resourceRecordService;
	private ObjectMapper _objectMapper;
	private ObjectNode _processor;
	private final RabbitTemplate _rabbitTemplate;

	@Autowired
	public DicomExtractorServiceImpl(final ResourceRecordService resourceRecordService,
			final RabbitTemplate rabbitTemplate, final Jackson2ObjectMapperFactoryBean objectMapperBuilderFactory,
			final LaunchCommand command) {
		_resourceRecordService = resourceRecordService;
		_rabbitTemplate = rabbitTemplate;
		_objectMapper = Objects.requireNonNull(objectMapperBuilderFactory.getObject());
		_processor = _objectMapper.createObjectNode();
		_processor.put("implementation", getClass().getName());
		command.call();

	}

	@RabbitListener(queues = { DicomExtractorConstants.CAPTURE_QUEUE_EVENT })
	public void listen(final CaptureRecord captureRecord) {
		log.info("Just got a record from the queue {}: {}", _captureQueueEvent, captureRecord);
		if (!StringUtils.equals(DicomExtractorConstants.DICOM_FORMAT, captureRecord.getFormat()))
			log.warn("I'm only supposed to handle DICOM, but I found a non-DICOM data format: {}",
					captureRecord.getFormat());
		ResourceRecord resourceRecord = null;
		try {
			resourceRecord = extract(captureRecord);
		} catch (Exception e) {
			resourceRecord = null;
			e.printStackTrace();
		}
		if (resourceRecord == null)
			_rabbitTemplate.convertAndSend(_dlxQueueEvent, captureRecord);
		else
			_rabbitTemplate.convertAndSend(_extractQueueEvent, resourceRecord);
	}

	@Override
	public ResourceRecord extract(CaptureRecord captureRecord) {
		final StopWatch stopWatch = StopWatch.createStarted();
		final StringWriter writer = new StringWriter(DicomExtractorConstants.STRING_BUFFER_SIZE);
		@NonNull
		final String location = captureRecord.getLocation();
		try (final InputStream input = new URI(location).toURL().openStream();
				final DicomInputStream inputStream = new DicomInputStream(input);
				final JsonGenerator generator = Json.createGenerator(writer)) {
			final Attributes attributes = inputStream.readDataset(-1, Tag.PixelData);
			attributes.removeSelected(
					Arrays.stream(attributes.tags()).filter(tag -> attributes.getVR(tag).isInlineBinary()).toArray());
			final JSONWriter jsonWriter = new JSONWriter(generator);
			jsonWriter.write(attributes);
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException(e);
		}
		final String dicom = writer.toString();
		stopWatch.stop();
		log.info("Completed extracting {} bytes of DICOM from file {} in {} ms", dicom.length(), location,
				stopWatch.getTime(TimeUnit.MILLISECONDS));
		try {
			ResourceRecord resourceRecord = ResourceRecord.builder().format(DicomExtractorConstants.DICOM_FORMAT)
					.location(location).processor(_processor).metadata(_objectMapper.readTree(dicom))
					.extractionStartTime(new Date(stopWatch.getStartTime())).extractionEndTime(new Date()).build();

			resourceRecord.setCreated(new Date());
			resourceRecord.setLastModified(new Date());

			return _resourceRecordService.createResourceRecord(resourceRecord);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void extractToQueue(DicomImage dicomImage) {
		ResourceRecord resourceRecord = extractDicomImage(dicomImage);
		resourceRecord.setCreated(new Date());
		resourceRecord.setLastModified(new Date());

		_rabbitTemplate.convertAndSend(DicomExtractorConstants.EXTRACT_QUEUE_EVENT, resourceRecord);
	}

	private ResourceRecord extractDicomImage(DicomImage dicomImage) {

		@NonNull
		String fileLocation = dicomImage.getLocation();
		final StopWatch stopWatch = StopWatch.createStarted();
		final StringWriter writer = new StringWriter(DicomExtractorConstants.STRING_BUFFER_SIZE);
		final String location = DicomExtractorConstants.FILE_START_TEXT + fileLocation;
		try (final InputStream input = new URL(location).openStream();
				final DicomInputStream inputStream = new DicomInputStream(input);
				final JsonGenerator generator = Json.createGenerator(writer)) {
			final Attributes attributes = inputStream.readDataset(-1, Tag.PixelData);
			attributes.removeSelected(
					Arrays.stream(attributes.tags()).filter(tag -> attributes.getVR(tag).isInlineBinary()).toArray());
			final JSONWriter jsonWriter = new JSONWriter(generator);
			jsonWriter.write(attributes);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		final String dicom = writer.toString();
		stopWatch.stop();
		log.info("Completed extracting {} bytes of DICOM from file {} in {} ms", dicom.length(), location,
				stopWatch.getTime(TimeUnit.MILLISECONDS));
		try {
			return _resourceRecordService.createResourceRecord(ResourceRecord.builder()
					.format(DicomExtractorConstants.DICOM_FORMAT).location(location).processor(_processor)
					.metadata(_objectMapper.readTree(dicom)).extractionStartTime(new Date(stopWatch.getStartTime()))
					.extractionEndTime(new Date()).build());
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
