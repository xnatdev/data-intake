package com.xnat.extractorapp.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

	@Value("${spring.rabbitmq.extract.event}")
	private String extractQueueEvent;

	@Value("${spring.rabbitmq.dlx.event}")
	private String dlxQueueEvent;

	@Value("${spring.rabbitmq.capture.event}")
	private String captureQueueEvent;

	@Bean
	public Queue extractQueue() {
		return new Queue(extractQueueEvent, true);
	}

	@Bean
	public Queue captureQueueEvent() {
		return new Queue(captureQueueEvent, true);
	}

	@Bean
	public Queue dlxQueueEvent() {
		return new Queue(dlxQueueEvent, true);
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

}
