package com.xnat.extractorapp;

import java.util.List;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class ExtractorAppApplication implements ApplicationRunner {

	public static void main(String[] arguments) {
		SpringApplication application = new SpringApplication(ExtractorAppApplication.class);
		application.run(arguments);
	}
	
	@Override
	public void run(ApplicationArguments arguments) throws Exception {
		if (arguments.containsOption("mode")) {
			final List<String> modes = arguments.getOptionValues("mode");
			if (modes.isEmpty()) {
				throw new RuntimeException("You must provide a mode with the --mode=<MODE> command-line parameter.");
			}
			if (modes.size() > 1) {
				throw new RuntimeException(
						"You must provide only one mode with the --mode=<MODE> command-line parameter.");
			}
		}
		if (arguments.containsOption("ae-title")) {
			final List<String> titles = arguments.getOptionValues("ae-title");
			if (titles.isEmpty()) {
				throw new RuntimeException(
						"You must provide an AE title with the --ae-title=<AE> command-line parameter.");
			}
			if (titles.size() > 1) {
				throw new RuntimeException(
						"You must provide only one AE title with the --ae-title=<AE> command-line parameter.");
			}
		}
		if (arguments.containsOption("port")) {
			final List<String> ports = arguments.getOptionValues("port");
			if (ports.isEmpty()) {
				throw new RuntimeException("You must provide an port with the --port=<PORT> command-line parameter.");
			}
			if (ports.size() > 1) {
				throw new RuntimeException(
						"You must provide only one port with the --port=<PORT> command-line parameter.");
			}
		}
		if (arguments.containsOption("service-name")) {
			final List<String> serviceNames = arguments.getOptionValues("service-name");
			if (serviceNames.isEmpty()) {
				throw new RuntimeException(
						"You must provide service name with the --service-name=<SN> command-line parameter.");
			}
			if (serviceNames.size() > 1) {
				throw new RuntimeException(
						"You must provide only one provide service name with the --service-name=<SN> command-line parameter.");
			}
		}

	}
}
