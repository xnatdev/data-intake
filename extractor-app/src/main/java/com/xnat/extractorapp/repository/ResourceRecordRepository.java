package com.xnat.extractorapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.xnat.extractorapp.entity.ResourceRecord;


@Repository
public interface ResourceRecordRepository extends JpaRepository<ResourceRecord, Long> {

}
