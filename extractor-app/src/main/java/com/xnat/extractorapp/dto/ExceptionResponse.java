package com.xnat.extractorapp.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse implements Serializable {
	private static final long serialVersionUID = -4220484279301938065L;
	private Integer code;
	private String message;

}
