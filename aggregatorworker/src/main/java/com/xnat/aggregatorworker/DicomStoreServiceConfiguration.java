/*
  dicom-store: org.nrg.xnat.dicom.workers.store.DicomStoreServiceConfiguration
  XNAT http://www.xnat.org Copyright (c) 2005-2020, Washington University
  School of Medicine All Rights Reserved
 
  Released under the Simplified BSD.
  */

package com.xnat.aggregatorworker;

import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xnat.aggregatorworker.LaunchCommand.Mode;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class DicomStoreServiceConfiguration {

	@Autowired
	public DicomStoreServiceConfiguration(final ApplicationArguments arguments) {
		log.info("Creating LaunchCommand from application arguments: {}",
				StringUtils.join(arguments.getSourceArgs(), " "));
		final Mode mode = Mode
				.valueOf(arguments.containsOption("mode") ? arguments.getOptionValues("mode").get(0) : "extract");
		final String aeTitle = arguments.containsOption("ae-title") ? arguments.getOptionValues("ae-title").get(0)
				: "XNAT";
		final int port = arguments.containsOption("port") ? Integer.parseInt(arguments.getOptionValues("port").get(0))
				: 8104;
		final List<String> nonOptionArgs = arguments.getNonOptionArgs();
		final String destination = nonOptionArgs.isEmpty() ? "store" : nonOptionArgs.get(0);
		
			_command = LaunchCommand.builder().mode(mode).aeTitle(aeTitle).port(port).destination(Paths.get(destination))
				.build();
	}

	@Bean
	public LaunchCommand launchCommand() {
		return _command;
	}
	
	private final LaunchCommand _command;
}
