/**
 * 
 */
package com.xnat.aggregatorworker.exception;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.extern.slf4j.Slf4j;

/**
 * @author afour
 *
 */
@Slf4j
public class ValueNotFoundException extends NotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7491609562835463747L;

	public ValueNotFoundException() {
		super();
	}
	
	public ValueNotFoundException(String path) {
		super();
		log.error("Field {} node found in json Node parsed.", path);
		System.err.println(String.format("Field {0} node found in json Node parsed.", path));
//		System.out.println(String.format("Field {} node found in json Node parsed.", path));
	}
	
	public ValueNotFoundException(JsonNode jsonNode, String path) {
		super();
		log.error("Field {} node found in json Node passed.", path);
	}

}
