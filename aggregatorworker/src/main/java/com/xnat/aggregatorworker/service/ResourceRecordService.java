package com.xnat.aggregatorworker.service;

import java.util.List;

import com.xnat.aggregatorworker.entity.ResourceRecord;

public interface ResourceRecordService {

	ResourceRecord createResourceRecord(ResourceRecord resourceRecord);

	ResourceRecord findResourceRecordById(Long id);
	
	List<ResourceRecord> getAllResourceRecords();
  
  }
 