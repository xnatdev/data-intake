/**
 * 
 */
package com.xnat.aggregatorworker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnat.aggregatorworker.entity.SessionRecord;
import com.xnat.aggregatorworker.repository.SessionRecordRepository;
import com.xnat.aggregatorworker.service.SessionRecordService;

/**
 * @author afour
 *
 */
@Service
public class SessionRecordServiceImpl implements SessionRecordService {

	
	@Autowired
	SessionRecordRepository sessionRecordRepository;
	
	@Override
	public SessionRecord createSessionRecord(SessionRecord sessionRecord) {
		return sessionRecordRepository.save(sessionRecord);
	}

}
