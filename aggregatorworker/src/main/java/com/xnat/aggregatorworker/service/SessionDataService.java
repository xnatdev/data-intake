/**
 * 
 */
package com.xnat.aggregatorworker.service;

import com.xnat.aggregatorworker.entity.SessionData;

/**
 * @author afour
 *
 */
public interface SessionDataService {
	public SessionData createSessionData(SessionData sessionData);
}
