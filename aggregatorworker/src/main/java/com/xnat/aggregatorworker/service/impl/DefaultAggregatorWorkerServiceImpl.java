/**
 * 
 */
package com.xnat.aggregatorworker.service.impl;

import static com.xnat.aggregatorworker.constants.DicomExtractorConstants.AGGREGATE_QUEUE_EVENT;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

//import org.dcm4che3.data.Tag;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.xnat.aggregatorworker.LaunchCommand;
import com.xnat.aggregatorworker.dto.AggregatorOutput;
import com.xnat.aggregatorworker.entity.Catalog;
import com.xnat.aggregatorworker.entity.Patient;
import com.xnat.aggregatorworker.entity.ResourceRecord;
import com.xnat.aggregatorworker.entity.Scanner;
import com.xnat.aggregatorworker.entity.Scan;
import com.xnat.aggregatorworker.entity.SessionData;
import com.xnat.aggregatorworker.entity.SessionRecord;
import com.xnat.aggregatorworker.service.AggregatorWorkerService;
import com.xnat.aggregatorworker.service.ResourceRecordService;
import com.xnat.aggregatorworker.service.SessionDataService;
import com.xnat.aggregatorworker.service.SessionRecordService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author afour
 *
 */
@Slf4j
@Service
public class DefaultAggregatorWorkerServiceImpl implements AggregatorWorkerService {

	private ObjectMapper _objectMapper;
	private ObjectNode _processor;
//	private RabbitTemplate _rabbitTemplate;
	ResourceRecordService _resourceRecordService;
	SessionRecordService _sessionRecordService;
	SessionDataService _sessionDataService;

	@Autowired
	public DefaultAggregatorWorkerServiceImpl(final Jackson2ObjectMapperFactoryBean objectMapperBuilderFactory,
			final LaunchCommand command, ResourceRecordService resourceRecordService, SessionDataService sessionDataService,
			SessionRecordService sessionRecordService) {
		_resourceRecordService = resourceRecordService;
		_sessionRecordService = sessionRecordService;
		_sessionDataService = sessionDataService;
		_objectMapper = Objects.requireNonNull(objectMapperBuilderFactory.getObject());
		_processor = _objectMapper.createObjectNode();
		_processor.put("implementation", getClass().getName());
		command.call();
	}

	@RabbitListener(queues = AGGREGATE_QUEUE_EVENT)
	public void listen(final AggregatorOutput aggregatorOutput) {
		log.info("Just got a record from the queue {}: {}", AGGREGATE_QUEUE_EVENT, aggregatorOutput);

		List<ResourceRecord> records = _resourceRecordService.getAllResourceRecords();
		if (records != null && !records.isEmpty())
			convertResourcesRecordtoSessionData(records, aggregatorOutput);

//			_rabbitTemplate.convertAndSend(AGGREGATE_QUEUE_EVENT, sessionRecord);

	}

	@Override
	public void convertResourcesRecordtoSessionData(List<ResourceRecord> resourceRecords,
			AggregatorOutput aggregatorOutput) {

		// filtering out resource records based on id and studyInstanceUid.
		resourceRecords = resourceRecords.parallelStream()
				.filter(r -> getJsonNodeValue(r.getMetadata(), "0020000D")
						.equalsIgnoreCase(aggregatorOutput.getStudyInstanceUid()))
				.filter(r -> r.getId().equals(aggregatorOutput.getId())).collect(Collectors.toList());

		if (resourceRecords != null && !resourceRecords.isEmpty()) {
			log.info("ResourceRecords : {} ", resourceRecords);

			int length = resourceRecords.size();
			int count = 0;
			LocalDate startDate = LocalDate.MAX;
			LocalTime startTime = LocalTime.MAX;
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss.SSSSSS");

			for (ResourceRecord resourceRecord : resourceRecords) {

				JsonNode jsonNode = resourceRecord.getMetadata();
				Set<Scan> scans = new HashSet<>();

				startDate = LocalDate.parse(getJsonNodeValue(jsonNode, "00080020"), dateFormatter).isBefore(startDate)
						? LocalDate.parse(getJsonNodeValue(jsonNode, "00080020"), dateFormatter)
						: startDate;
				startTime = LocalTime.parse(getJsonNodeValue(jsonNode, "00080031"), timeFormatter).isBefore(startTime)
						? LocalTime.parse(getJsonNodeValue(jsonNode, "00080031"), timeFormatter)
						: startTime;

				Catalog catalog = Catalog.builder().format("DICOM").label(getJsonNodeValue(jsonNode, "00080018"))
						.content(getJsonNodeValue(jsonNode, "00200013")).resources(resourceRecords).build();

				Scan scan = Scan.builder().seriesNumber(getJsonNodeValue(jsonNode, "00200011"))
						.startDate(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(startDate))
						.startTime(startTime.format(DateTimeFormatter.ISO_TIME))
						.seriesInstanceUid(getJsonNodeValue(jsonNode, "0020000E"))
						.seriesDescription(getJsonNodeValue(jsonNode, "0008103E"))
						.modality(getJsonNodeValue(jsonNode, "00080060")).catalog(catalog)
						.sopClassUid(getJsonNodeValue(jsonNode, "00080016")).build();

				log.info("Scan {} : {}", ++count, scan);
				scans.add(scan);

				// creating patient, scanner & sessionData object at last iteration of for-each.
				if (count == length) {
					Patient patient = new Patient(getJsonNodeValue(jsonNode, "00100020"),
							jsonNode.findPath("00100010").findValue("Value").findValue("Alphabetic").asText(),
							getJsonNodeValue(jsonNode, "00100040"), getJsonNodeValue(jsonNode, "00101010"),
							jsonNode.findPath("00101030").findValue("Value").path(0).asDouble(0.00));

					log.info("Patient: {}", patient);

					Scanner scanner = new Scanner(getJsonNodeValue(jsonNode, "00080070"),
							getJsonNodeValue(jsonNode, "00081090"), getJsonNodeValue(jsonNode, "00181020"));

					log.info("Scanner: {}", scanner);

					SessionData sessionData = SessionData.builder()
							.studyInstanceUid(getJsonNodeValue(jsonNode, "0020000D"))
							.studyDescription(getJsonNodeValue(jsonNode, "00081030"))
							.accessionNumber(getJsonNodeValue(jsonNode, "00080050")).scans(scans)
							.date(getJsonNodeValue(jsonNode, "00080020")).time(getJsonNodeValue(jsonNode, "00080030"))
							.patient(patient).scanner(scanner).build();

//					sessionData = _sessionDataService.createSessionData(sessionData);
					
					log.info("SessionData: {}", sessionData);

//			sessionData.setScans(scans);

//		aggregatorEntity.setStudyId(jsonNode.findPath("00200010").findValue("Value").path(0).asText());
//		aggregatorEntity.setSessionType(jsonNode.findPath("0020000D").findValue("Value").path(0).asText());Not Present in dicom file
//		aggregatorEntity.setSubjectId(jsonNode.findPath("0020000D").findValue("Value").path(0).asText());Not Present in dicom file

//		aggregatorEntity.setAcquisitionSite(acquisitionSite); Not Present in dicom file, MRAcquisitionType or AcquisitionMatrix
//		aggregatorEntity.setInstitutionName(institutionName); Not Present in dicom file

//		aggregatorEntity.setTransferSyntaxUid(jsonNode.findPath("00020010").findValue("Value").path(0).asText()); not present in resource record

					ObjectMapper mapper = new ObjectMapper();
					SessionRecord sessionRecord = SessionRecord.builder().processor(_processor)
							.metadata(mapper.convertValue(sessionData, JsonNode.class)).build();

					sessionRecord.setCreated(new Date());
					sessionRecord.setLastModified(new Date());

					_sessionRecordService.createSessionRecord(sessionRecord);
				}
			}
		}
	}

	private String getJsonNodeValue(JsonNode node, String nodeId) {

		final JsonNode nodeData = node.findPath(nodeId);

		if (nodeData != null) {
			final JsonNode nodeValue = nodeData.findValue("Value").path(0);

			return nodeValue.asText("value not found");
		}
		return "";
	}
}
