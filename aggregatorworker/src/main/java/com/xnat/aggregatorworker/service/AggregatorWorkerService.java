/**
 * 
 */
package com.xnat.aggregatorworker.service;

import java.util.List;

import com.xnat.aggregatorworker.dto.AggregatorOutput;
import com.xnat.aggregatorworker.entity.ResourceRecord;

/**
 * @author afour
 *
 */
public interface AggregatorWorkerService {
	public void listen(final AggregatorOutput aggregatorOutput);
	
	void convertResourcesRecordtoSessionData(List<ResourceRecord> resourceRecords, AggregatorOutput aggregatorOutput);
}
