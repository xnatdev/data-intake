/**
 * 
 */
package com.xnat.aggregatorworker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnat.aggregatorworker.entity.SessionData;
import com.xnat.aggregatorworker.repository.SessionDataRepository;
import com.xnat.aggregatorworker.service.SessionDataService;

/**
 * @author afour
 *
 */
@Service
public class SessionDataServiceImpl implements SessionDataService {

	
	@Autowired
	SessionDataRepository sessionDataRepository;
	
	@Override
	public SessionData createSessionData(SessionData sessionData) {
		return sessionDataRepository.save(sessionData);
	}

}
