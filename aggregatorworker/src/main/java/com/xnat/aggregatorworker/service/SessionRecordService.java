/**
 * 
 */
package com.xnat.aggregatorworker.service;

import com.xnat.aggregatorworker.entity.SessionRecord;

/**
 * @author afour
 *
 */
public interface SessionRecordService {
	public SessionRecord createSessionRecord(SessionRecord sessionRecord);
}
