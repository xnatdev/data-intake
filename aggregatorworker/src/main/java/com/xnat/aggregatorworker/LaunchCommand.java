/*
  dicom-store: org.nrg.xnat.dicom.workers.store.LaunchCommand XNAT
  http://www.xnat.org Copyright (c) 2005-2020, Washington University School of
  Medicine All Rights Reserved
 
  Released under the Simplified BSD.
  */

package com.xnat.aggregatorworker;

import java.nio.file.Path;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

// TODO: picocli initializes *after* beans are created, so couldn't get thatto work. It'd be nice to make that work properly.

@Command(name = "launch", description = "Launches the DICOM C-STORE receiver", mixinStandardHelpOptions = true)
@Getter
@Builder
@Accessors(prefix = "_")
@Slf4j
public class LaunchCommand implements Callable<Integer> {

	@Override
	public Integer call() {
		log.info("The launch command was called with the following options and parameters:");
		switch (getMode()) {
		case capture:
			log.info(" * Mode:         capture");
			log.info(" * AE title:     {}", StringUtils.defaultIfBlank(getAeTitle(), "<NA>"));
			log.info(" * Port:         {}", getPort() != 0 ? getPort() : "<NA>");
			log.info(" * Service name: {}", StringUtils.defaultIfBlank(getServiceName(), "<NA>"));
			log.info(" * Destination:  {}", getDestination() != null ? getDestination() : "<NA>");
			break;

		case extract:
			log.info(" * Mode: extract");
			break;

		case aggregate:
			log.info(" * Mode: aggregate");
			break;

		case rest:
			log.info(" * Mode: rest");
			break;
		}
		return 0;
	}

	public enum Mode {
		capture, extract, aggregate, rest
	}

	@Option(names = { "-m",
			"--mode" }, paramLabel = "<MODE>", description = "Specifies the mode for this application. The available modes are \"capture\", \"extract\", and \"aggregate\".", defaultValue = "capture")
	private final Mode _mode;

	@Option(names = { "-aet",
			"--ae-title" }, paramLabel = "<AE>", defaultValue = "XNAT", description = "Specifies the AE title for the DICOM receiver. Defaults to \"XNAT\".")
	private final String _aeTitle;

	@Option(names = { "-p",
			"--port" }, paramLabel = "<PORT>", defaultValue = "8104", description = "Specifies the port for the DICOM receiver. Defaults to 8104.")
	private final int _port;

	@Option(names = { "-s",
			"--service" }, paramLabel = "<SN>", defaultValue = "cStore", description = "Specifies the service name for the DICOM receiver. Defaults to \"cStore\".")
	private final String _serviceName;

	@Parameters(index = "0", paramLabel = "<FOLDER>", defaultValue = "store", description = "The folder where incoming data should be stored.")
	private final Path _destination;
}
