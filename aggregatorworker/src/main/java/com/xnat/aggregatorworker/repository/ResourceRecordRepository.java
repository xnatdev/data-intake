package com.xnat.aggregatorworker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xnat.aggregatorworker.entity.ResourceRecord;


@Repository
public interface ResourceRecordRepository extends JpaRepository<ResourceRecord, Long> {

}
