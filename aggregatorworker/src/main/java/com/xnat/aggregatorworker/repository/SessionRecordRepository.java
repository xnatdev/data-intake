/**
 * 
 */
package com.xnat.aggregatorworker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xnat.aggregatorworker.entity.SessionRecord;

/*import org.nrg.xnat.sentryapp.entity.CaptureRecord;
import org.springframework.data.jpa.repository.JpaRepository;*/

/**
 * @author afour
 *
 */
@Repository
public interface SessionRecordRepository extends JpaRepository<SessionRecord, Long> 
{

}
