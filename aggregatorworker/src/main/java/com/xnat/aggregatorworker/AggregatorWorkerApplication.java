package com.xnat.aggregatorworker;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class AggregatorWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AggregatorWorkerApplication.class, args);
	}

}
