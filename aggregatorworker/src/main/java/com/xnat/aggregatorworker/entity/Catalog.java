/**
 * 
 */
package com.xnat.aggregatorworker.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(schema = "xnat")
public class Catalog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;// 1,
	
//	 @NonNull
	 private String label;//sopInstanceUid
	
//	 @NonNull
	 private String content;//instanceNumber
	
//	 @NonNull
	 private String format;
	
	 @OneToMany
	 @JsonIgnore
	 private List<ResourceRecord> resources;
	
	
}
