/**
 * 
 */
package com.xnat.aggregatorworker.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "xnat")
public class SessionData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String studyInstanceUid;
	private String studyId;
	private String studyDescription;
	private String accessionNumber;
	private String sessionType;
	private String subjectId;
	private Patient patient;
	private String date;
	private String time;
	private String acquisitionSite;
	private Scanner scanner;
	private String institutionName;
	private String transferSyntaxUid;

	@OneToMany(mappedBy = "sessionData", cascade = CascadeType.ALL)
	private Set<Scan> scans;
}
