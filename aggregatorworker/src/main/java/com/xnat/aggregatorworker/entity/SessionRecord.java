/**
 * 
 */
package com.xnat.aggregatorworker.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(schema = "xnat", name = "session_record")
public class SessionRecord extends BaseMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5020916628084074497L;

	@Type(type = "JsonB")
	@Column(columnDefinition = "JsonB")
	private JsonNode processor;
	@Type(type = "JsonB")
	@Column(columnDefinition = "JsonB")
	private JsonNode metadata;

//	private String studyInstanceUid;
//	private String studyDescription;
//
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "session_data_study_id", referencedColumnName = "studyId")
//	private SessionData sessionData;

//	private List<Integer> scans;

}
