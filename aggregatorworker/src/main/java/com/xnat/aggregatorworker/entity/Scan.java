/**
 * 
 */
package com.xnat.aggregatorworker.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "xnat")
public class Scan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;// 1,
	
	private String seriesNumber;// "1",
	private String seriesInstanceUid;// "1.3.6.1.4.1.14519.5.2.1.2103.7010.104268351572585692175223925100",
	private String seriesDescription;// "twist_20s_dyn_TRA (h20 ex B17)_TT=533.4s]",
	private String modality;// "MR",
	private String startDate;// "20200104",
	private String startTime;// "080614.325000",
	private String frames;// "128",
	private String sopClassUid;// "MRImageStorage",

	@ManyToOne
	@JoinColumn(name = "scan_id", nullable = false)
	private SessionData sessionData;
	
	
	 @OneToOne
	 private Catalog catalog;
}
