/**
 * 
 */
package com.xnat.aggregatorworker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author afour
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregatorOutput {
	Long id;
	String studyInstanceUid;
}
